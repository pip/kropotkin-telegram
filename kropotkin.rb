# frozen_string_literal: true

require "telegram/bot"
require "pry"

token = ENV["BOT_TOKEN"]

def reply?
  [true, false, true].sample
end

Telegram::Bot::Client.run(token) do |bot|
  bot.listen do |message|
    # Saludar a todes
    message.new_chat_members.each do |member|
      bot.api.send_message(chat_id: message.chat.id,
                           reply_to_message_id: message.message_id,
                           text:    "Bienvenide, #{member.first_name} o/ " \
                                    "Qué pronombres usas? " \
                                    "Te invitamos a leer nuestros códigos para compartir: " \
                                    "https://utopia.partidopirata.com.ar/zines/codigos_para_compartir.html")
    end

    next unless reply?

    respuesta = case message.text
      when /open ?source/i then "no querrás decir software libre?"
      when /ha(y|bria|bría) que/i then "che que buena idea, por qué no la haces?"
      when /:[c(]/i then "te mando un abrazo :#{['c', '('].sample}"
      when /windows/i then "eso en linux/GNU no pasa xD"
      when /gat(it)?[oiaex]s?/i then "LXS GATITXS SON LO MEJOR"
      when /\b(yuta|polic[ií]a|rati)\b/i then ["muerte a la #{$1}", "never yuta"].sample
      when /copi(a|ar?)/i then "copiar no es robar!"
      when /\Wbot\W/i then ['a quién le habla?', 'hay unx bot por acá? :O', '¬¬', 'qué estás haciendo, dave?'].sample
      when /\Wtorrent\W/i then ["compartir es bueno", "copiar no es robar", "torrent o patria","si no torrenteamos, la cultura se netflixea"].sample
    end

    next unless respuesta

    sleep rand(9) + 1
    bot.api.send_message(chat_id: message.chat.id, text: respuesta, reply_to_message_id: message.message_id)
  rescue Telegram::Bot::Exceptions::ResponseError => e
    puts "ResponseError, #{e.message}"
  end
end
